# Use the official k6 image from the Docker Hub
FROM grafana/k6:latest

# Set the working directory inside the container
WORKDIR /home/aditya/Downloads/docker-k6-grafana-influxdb

# Copy the k6 script to the working directory
COPY . .

# Run k6 with the provided script by default
CMD ["run", "/scripts/ewoks.js"]
